package init_shop

import (
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

var (
	Apple = model.Product{
		Id:          0,
		Label:       "Apple",
		Description: "An apple a day keeps the doctor away.",
		Image:       "",
		Price:       3,
	}

	Banana = model.Product{
		Id:          1,
		Label:       "Banana",
		Description: "Believe it or not, bananas do contain a small quantity of Musa Sapientum bananadine, which is a mild, short-lasting psychedelic.",
		Image:       "",
		Price:       5,
	}

	Pear = model.Product{
		Id:          2,
		Label:       "Pear",
		Description: "Slice a pear and you will find that its flesh is incandescent white. It glows with inner light. Those who carry a knife and a pear are never afraid of the dark.",
		Image:       "",
		Price:       8,
	}

	Orange = model.Product{
		Id:          3,
		Label:       "Orange",
		Description: "Try not thinking of peeling an orange. Try not imagining the juice running down your fingers, the soft inner part of the peel. The smell. Try and you can't. The brain doesn't process negatives.",
		Image:       "",
		Price:       13,
	}

	BananaPearSet = model.Product{
		Id:          4,
		Label:       "Set of 4 pears and 2 bananas",
		Description: "The fruits orchestra",
		Image:       "",
		Price:       4*Pear.Price + 2*Banana.Price,
	}

	ShopInitialized bool
)

func init() {
	mock := config.GetConfig().Mock

	if !mock.Redis && impl.GetRedisClient() == nil ||
		!mock.PostgreSQL && impl.GetPostgreSqlClient() == nil ||
		!mock.MongoDB && impl.GetMongoDbClient() == nil {
		return
	}

	var err error

	for _, product := range []*model.Product{&Apple, &Banana, &Pear, &Orange} {
		if _, err = dao.GetProductDao().GetById(product.Id); err != nil {
			if _, err = dao.GetProductDao().Insert(*product); err != nil {
				return
			}
		}
	}

	ShopInitialized = true
}
