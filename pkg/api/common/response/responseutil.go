package response

type Response struct {
	Body       interface{}
	StatusCode int
}
