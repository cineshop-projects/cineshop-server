package cart

import (
	"fmt"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	price_rules "gitlab.com/cineshop-projects/cineshop-server/pkg/price-rules"
)

func UpdateCartPrice(cart *model.Cart) error {
	if cart == nil {
		return fmt.Errorf("not found")
	}

	err := price_rules.GetPriceRulesEngine().EvaluateAndUpdate(cart)
	if err != nil {
		return err
	}

	return nil
}
