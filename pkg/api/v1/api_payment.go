/*
 * Cineshop
 *
 * Cineshop's fruit shop
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package v1

import (
	"encoding/json"
	"net/http"
	"strings"
)

// A PaymentApiController binds http requests to an api service and writes the service results to the http response
type PaymentApiController struct {
	service PaymentApiServicer
	private bool
	logged  bool
}

// NewPaymentApiController creates a default api controller
func NewPaymentApiController(s PaymentApiServicer) Router {
	return &PaymentApiController{service: s}
}

// Routes returns all of the api route for the PaymentApiController
func (c *PaymentApiController) Routes() Routes {
	return Routes{
		{
			"ValidatePayment",
			strings.ToUpper("Post"),
			"/v1/payment/validation",
			c.ValidatePayment,
		},
	}
}

// WithAuthentication flags this api as private
func (c *PaymentApiController) WithAuthentication() Router {
	c.private = true
	return c
}

// IsPrivate returns true if this api requires authentication, false otherwise
func (c *PaymentApiController) IsPrivate() bool {
	return c.private
}

// WithLog flags this api as using the logger middleware
func (c *PaymentApiController) WithLog() Router {
	c.logged = true
	return c
}

// IsLogged returns true if this api uses the logger middleware, false otherwise
func (c *PaymentApiController) IsLogged() bool {
	return c.logged
}

// ValidatePayment - Validates the status of the purchase upon the response of the payment gateway.
func (c *PaymentApiController) ValidatePayment(w http.ResponseWriter, r *http.Request) {
	order := &Order{}
	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	userId, err := ExtractUserId(r)
	if err != nil {
		if err == ValueInternalSeverError {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		return
	}

	result := c.service.ValidatePayment(userId, *order)

	EncodeJSONResponse(result.Body, &result.StatusCode, w)
}
