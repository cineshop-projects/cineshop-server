/*
 * Cineshop
 *
 * Cineshop's fruit shop
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package v1

import (
	"log"
	"net/http"
	"net/http/httputil"
	"time"
)

type loggedResponseWriter struct {
	http.ResponseWriter
	statusCode int
	payload    []byte
}

func NewLoggedResponseWriter(w http.ResponseWriter) *loggedResponseWriter {
	return &loggedResponseWriter{
		ResponseWriter: w,
		statusCode:     http.StatusOK,
	}
}

func (lrw *loggedResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func (lrw *loggedResponseWriter) Write(payload []byte) (int, error) {
	lrw.payload = payload
	return lrw.ResponseWriter.Write(payload)
}

func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dump, err := httputil.DumpRequest(r, true)
		if err != nil {
			log.Printf(
				"REQ  %s: \"%s\" (%s) [%v]",
				r.Method,
				r.RequestURI,
				name,
				err,
			)
		} else {
			log.Printf(
				"REQ  %s: \"%s\" (%s)\n%v",
				r.Method,
				r.RequestURI,
				name,
				string(dump),
			)
		}

		lw := NewLoggedResponseWriter(w)

		start := time.Now()

		inner.ServeHTTP(lw, r)

		log.Printf(
			"RESP %s: \"%s\" (%s) [%v] [%s]\n%v",
			r.Method,
			r.RequestURI,
			name,
			lw.statusCode,
			time.Since(start),
			string(lw.payload),
		)
	})
}
