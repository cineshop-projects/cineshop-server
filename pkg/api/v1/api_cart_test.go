package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	init_shop "gitlab.com/cineshop-projects/cineshop-server/pkg/init-shop"
	"golang.org/x/crypto/bcrypt"
)

var (
	cartApiController = &CartApiController{service: NewCartApiService()}
)

func TestCartCouponPost_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	_, err = dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartcouponinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"code":"CouponCouponInternalServerErrorContext"}`)
	err = dao.GetCouponDao().Insert("CouponCouponInternalServerErrorContext")
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/v1/cart/coupon", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.ApplyCoupon)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartCouponPost_NoContent(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartcouponnocontent@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// Apply a coupon, the next coupons wil not be applied, thus the StatusNoContent
	cart, err := dao.GetCartDao().GetByUserId(model.UserId(user.Id))
	if err != nil {
		t.Fatal(err)
	}
	cart.Coupon = "COUPON0987654321"
	_, err = dao.GetCartDao().Upsert(model.UserId(user.Id), cart)
	if err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"code":"CouponCouponNoContent"}`)
	err = dao.GetCouponDao().Insert("CouponCouponNoContent")
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/v1/cart/coupon", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.ApplyCoupon)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body, "")
	}
}

func TestCartCouponPost_BadRequest(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartcouponbadrequest@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"code":"CouponCouponBadRequest"}`)

	// The coupon is not stored, thus the StatusBadRequest

	req, err := http.NewRequest("POST", "/v1/cart/coupon", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.ApplyCoupon)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusBadRequest
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartCouponPost_Created(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartcouponcreated@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"code":"CouponCouponCreated"}`)
	err = dao.GetCouponDao().Insert("CouponCouponCreated")
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/v1/cart/coupon", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.ApplyCoupon)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body, "")
	}
}

func TestCartDelete_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	_, err = dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartdeleteinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("DELETE", "/v1/cart", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.DeleteCart)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartDelete_NotModified_Default(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartdeletenotmodifieddefault@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart is still in the default state, thus the StatusNotModified

	req, err := http.NewRequest("DELETE", "/v1/cart", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.DeleteCart)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotModified {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotModified)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartDelete_NotModified(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartdeletenotmodified@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The stored cart is empty, thus the StatusNotModified
	cart, err := dao.GetCartDao().GetByUserId(model.UserId(user.Id))
	if err != nil {
		t.Fatal(err)
	}
	_, err = dao.GetCartDao().Upsert(model.UserId(user.Id), cart)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("DELETE", "/v1/cart", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.DeleteCart)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotModified {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotModified)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartDelete_NoContent(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartdeletenocontent@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	req, err := http.NewRequest("DELETE", "/v1/cart", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.DeleteCart)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartGet_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	_, err = dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartgetinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", "/v1/cart", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.GetCart)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartGet_OK(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartgetok@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	req, err := http.NewRequest("GET", "/v1/cart", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartApiController.GetCart)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual Cart
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	expectedId := init_shop.Apple.Id
	if actual.Items[0].Id != expectedId {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Items[0].Id, expectedId)
	}

	var expectedQty int32 = 13
	if actual.Items[0].Quantity != expectedQty {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Items[0].Quantity, expectedQty)
	}
}
