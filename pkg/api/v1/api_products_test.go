package v1

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	init_shop "gitlab.com/cineshop-projects/cineshop-server/pkg/init-shop"
)

var (
	productsApiController = &ProductsApiController{service: NewProductsApiService()}
)

func TestProductsGet_OK(t *testing.T) {
	req, err := http.NewRequest("GET", "/v1/products", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := newTestRouter(productsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual []Product
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected = 4
	if len(actual) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			len(actual), expected)
	}
}

func TestProductsGet_OK_SortAsc(t *testing.T) {
	req, err := http.NewRequest("GET", "/v1/products", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	q := req.URL.Query()
	q.Set("sort", "asc")
	req.URL.RawQuery = q.Encode()

	r := httptest.NewRecorder()

	handler := newTestRouter(productsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual []Product
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expectedCount = 4
	if len(actual) != expectedCount {
		t.Errorf("handler returned unexpected body: got %v want %v",
			len(actual), expectedCount)
	}

	var expectedLabels []string
	expectedLabels = append(expectedLabels, init_shop.Apple.Label, init_shop.Banana.Label, init_shop.Orange.Label, init_shop.Pear.Label)
	for i, actualProduct := range actual {
		if actualProduct.Label != expectedLabels[i] {
			t.Errorf("handler returned unexpected body: got %v want %v",
				actualProduct.Label, expectedLabels[i])
		}
	}
}

func TestProductsGet_OK_SortDesc(t *testing.T) {
	req, err := http.NewRequest("GET", "/v1/products", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	q := req.URL.Query()
	q.Set("sort", "desc")
	req.URL.RawQuery = q.Encode()

	r := httptest.NewRecorder()

	handler := newTestRouter(productsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual []Product
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expectedCount = 4
	if len(actual) != expectedCount {
		t.Errorf("handler returned unexpected body: got %v want %v",
			len(actual), expectedCount)
	}

	var expectedLabels []string
	expectedLabels = append(expectedLabels, init_shop.Pear.Label, init_shop.Orange.Label, init_shop.Banana.Label, init_shop.Apple.Label)
	for i, actualProduct := range actual {
		if actualProduct.Label != expectedLabels[i] {
			t.Errorf("handler returned unexpected body: got %v want %v",
				actualProduct.Label, expectedLabels[i])
		}
	}
}

func TestProductsProductIdGet_NotFound(t *testing.T) {
	// The product having id 13 does not exist, thus StatusNotFound
	req, err := http.NewRequest("GET", "/v1/products/13", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := newTestRouter(productsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestProductsProductIdGet_OK(t *testing.T) {
	req, err := http.NewRequest("GET", "/v1/products/0", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := newTestRouter(productsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual Product
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	expectedLabel := init_shop.Apple.Label
	if actual.Label != expectedLabel {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Label, expectedLabel)
	}
}
