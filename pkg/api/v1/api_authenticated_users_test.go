package v1

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	"golang.org/x/crypto/bcrypt"
)

var (
	authenticatedUsersApiController = &AuthenticatedUsersApiController{service: NewAuthenticatedUsersApiService()}
)

func TestUsersSignoutPost_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "logoutinternalservererror@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	token, err := GenerateToken(user)
	if err != nil {
		t.Fatal(err)
	}

	_, err = jwt.Parse(string(token), GetAuthMiddleware().Options.ValidationKeyGetter)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/v1/users/signout", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The userToken is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(authenticatedUsersApiController.Signout)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestUsersSignoutPost_NotFound(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "logoutnotfound@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	token, err := GenerateToken(user)
	if err != nil {
		t.Fatal(err)
	}

	// The token is not stored, thus the StatusNotFound

	userToken, err := jwt.Parse(string(token), GetAuthMiddleware().Options.ValidationKeyGetter)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/v1/users/signout", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), TokenProperty, userToken))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(authenticatedUsersApiController.Signout)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestUsersSignoutPost_OK(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "logoutok@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	token, err := GenerateToken(user)
	if err != nil {
		t.Fatal(err)
	}

	err = dao.GetTokenDao().Insert(token)
	if err != nil {
		t.Fatal(err)
	}

	userToken, err := jwt.Parse(string(token), GetAuthMiddleware().Options.ValidationKeyGetter)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("POST", "/v1/users/signout", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), TokenProperty, userToken))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(authenticatedUsersApiController.Signout)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body, "")
	}
}
