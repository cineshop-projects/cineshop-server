package v1

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	"golang.org/x/crypto/bcrypt"
)

var (
	anonymousUsersApiController = &AnonymousUsersApiController{service: NewAnonymousUsersApiService()}
)

func TestUsersSigninPost_NotFound(t *testing.T) {
	var jsonStr = []byte(`{"email":"loginnotfound@test.test","password":"admin1234"}`)

	req, err := http.NewRequest("POST", "/v1/users/signin", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(anonymousUsersApiController.Signin)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestUsersSigninPost_Unauthorized(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("1234567890"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	if _, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "loginunauthorized@test.test",
		Password:  string(password),
	}); err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"email":"loginunauthorized@test.test","password":"admin1234"}`)

	req, err := http.NewRequest("POST", "/v1/users/signin", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(anonymousUsersApiController.Signin)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusUnauthorized
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestUsersSigninPost_OK(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "loginok@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"email":"loginok@test.test","password":"admin1234"}`)

	req, err := http.NewRequest("POST", "/v1/users/signin", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(anonymousUsersApiController.Signin)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual SignInResponse
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	if actual.FirstName != user.FirstName {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.FirstName, user.FirstName)
	}

	if actual.LastName != user.LastName {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.LastName, user.LastName)
	}

	if actual.Email != user.Email {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Email, user.Email)
	}

	if actual.AccessToken == "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.AccessToken, "non empty string")
	}
}

func TestUsersSignupPost_Conflict(t *testing.T) {
	var jsonStr = []byte(`{"email":"registerconflict@test.test","password":"admin1234","firstName":"admin","lastName":"conflict"}`)

	req, err := http.NewRequest("POST", "/v1/users/signup", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(anonymousUsersApiController.Signup)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	req, err = http.NewRequest("POST", "/v1/users/signup", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r = httptest.NewRecorder()

	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusConflict {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusConflict
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestUsersSignupPost_OK(t *testing.T) {
	var jsonStr = []byte(`{"email":"registerok@test.test","password":"admin1234","firstName":"admin","lastName":"ok"}`)

	req, err := http.NewRequest("POST", "/v1/users/signup", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(anonymousUsersApiController.Signup)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual SignInResponse
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	if actual.FirstName != "admin" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.FirstName, "admin")
	}

	if actual.LastName != "ok" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.LastName, "ok")
	}

	if actual.Email != "registerok@test.test" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Email, "registerok@test.test")
	}

	if actual.AccessToken == "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.AccessToken, "non empty string")
	}
}
