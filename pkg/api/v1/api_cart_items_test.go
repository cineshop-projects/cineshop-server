package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	init_shop "gitlab.com/cineshop-projects/cineshop-server/pkg/init-shop"
	"golang.org/x/crypto/bcrypt"
)

var (
	cartItemsApiController = &CartItemsApiController{service: NewCartItemsApiService()}
)

func TestCartItemsItemIdDelete_BadRequest_Path(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsdeletebadrequestpath@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	req, err := http.NewRequest("DELETE", "/v1/cart/items", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartItemsApiController.DeleteCartItem)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdDelete_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsdeleteinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	req, err := http.NewRequest("DELETE", "/v1/cart/items/0", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdDelete_BadRequest(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsdeletebadrequest@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	req, err := http.NewRequest("DELETE", "/v1/cart/items/BAD_ID", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusBadRequest
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdDelete_NotFound_Empty(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsdeletenotfoundempty@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart is still empty, thus the StatusNotFound

	req, err := http.NewRequest("DELETE", "/v1/cart/items/0", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdDelete_NotFound(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsdeletenotfound@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart contains only apples (id = 0), thus the StatusNotFound
	addApples(t, user.Id)

	req, err := http.NewRequest("DELETE", "/v1/cart/items/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdDelete_NoContent(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsdeletenocontent@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	req, err := http.NewRequest("DELETE", "/v1/cart/items/0", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPut_BadRequest_Path(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputbadrequestpath@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	var jsonStr = []byte(`{"quantity":1}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := http.HandlerFunc(cartItemsApiController.UpdateCartItem)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPut_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	var jsonStr = []byte(`{"quantity":1}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items/0", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPut_BadRequest(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputbadrequest@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	var jsonStr = []byte(`{"quantity":1}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items/BAD_ID", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusBadRequest
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdPut_NotFound_Empty(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputnotfoundempty@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart is still empty, thus the StatusNotFound

	var jsonStr = []byte(`{"quantity":1}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items/0", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdPut_NotFound(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputnotfound@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart contains only apples (id = 0), thus the StatusNotFound
	addApples(t, user.Id)

	var jsonStr = []byte(`{"quantity":1}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items/1", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdPut_NotModified(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputnotmodified@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	// The cart already contains 13 apples, thus the StatusNotModified
	var jsonStr = []byte(`{"quantity":13}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items/0", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotModified {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotModified)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPut_NoContent(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemsputnocontent@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	var jsonStr = []byte(`{"quantity":1}`)

	req, err := http.NewRequest("PUT", "/v1/cart/items/0", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPost_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	_, err = dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemspostinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	var jsonStr = []byte(`{"productId":0}`)

	req, err := http.NewRequest("POST", "/v1/cart/items", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPost_NotFound(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemspostnotfound@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The product having id 13 does not exist, thus the StatusNotFound
	var jsonStr = []byte(`{"productId":13}`)

	req, err := http.NewRequest("POST", "/v1/cart/items", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusNotFound
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestCartItemsItemIdPost_Created(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemspostcreated@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart does not contain apples (id = 0), thus the StatusCreated
	var jsonStr = []byte(`{"productId":0}`)

	req, err := http.NewRequest("POST", "/v1/cart/items", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestCartItemsItemIdPost_NoContent(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "cartitemspostnocontent@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	// The cart already contains apples (id = 0), thus the StatusNoContent
	var jsonStr = []byte(`{"productId":0}`)

	req, err := http.NewRequest("POST", "/v1/cart/items", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(cartItemsApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func addApples(t *testing.T, userId string) model.Cart {
	cart, err := dao.GetCartDao().GetByUserId(model.UserId(userId))
	if err != nil {
		t.Fatal(err)
	}
	cart.Items = make(model.Items)
	cart.Items[init_shop.Apple.Id] = model.Item{
		Product:  init_shop.Apple,
		Quantity: 13,
	}
	cart, err = dao.GetCartDao().Upsert(model.UserId(userId), cart)
	if err != nil {
		t.Fatal(err)
	}
	return cart
}

func newTestRouter(router Router) *mux.Router {
	return NewRouter(router)
}
