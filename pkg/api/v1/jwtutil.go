package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/cineshop-projects/cineshop-server/pkg/config"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

const (
	TokenProperty       = "token"
	UserDetailsProperty = "user-details"
)

var (
	ValueInternalSeverError = fmt.Errorf("internal server error")
)

func GenerateToken(user model.User) (model.Token, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":    user.Id,
		"email": user.Email,
		"iat":   time.Now(),
	})

	stringToken, err := token.SignedString([]byte(config.GetConfig().JWTSecret))
	return model.Token(stringToken), err
}

func GetAuthMiddleware() *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(config.GetConfig().JWTSecret), nil
		},
		UserProperty:  TokenProperty,
		SigningMethod: jwt.SigningMethodHS256,
		ErrorHandler: func(w http.ResponseWriter, r *http.Request, err string) {
			onError(w, r, err, http.StatusUnauthorized)
		},
	})
}

func WithUserDetails(handler http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userToken := r.Context().Value(TokenProperty)
		token, ok := userToken.(*jwt.Token)
		if !ok {
			onError(w, r, "Failed to retrieve authorization token", http.StatusInternalServerError)
			return
		}

		exist, err := dao.GetTokenDao().Exists(model.Token(token.Raw))
		if err != nil || !exist {
			onError(w, r, "Invalid authorization token", http.StatusUnauthorized)
			return
		}

		var email string
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			email = claims["email"].(string)
		}

		var newRequest *http.Request
		user, err := dao.GetUserDao().GetByEmail(email)
		if err != nil {
			if err == model.EntityNotFound {
				onError(w, r, "Unauthorized user", http.StatusUnauthorized)
			} else {
				onError(w, r, "Failed to retrieve authorized user", http.StatusInternalServerError)
			}
			return
		} else {
			newRequest = r.WithContext(context.WithValue(r.Context(), UserDetailsProperty, user))
			*r = *newRequest
		}

		handler.ServeHTTP(w, r)
	}
}

func ExtractUserId(r *http.Request) (model.UserId, error) {
	userDetails := r.Context().Value(UserDetailsProperty)
	user, ok := userDetails.(model.User)
	if !ok {
		if valueErr, ok := userDetails.(error); ok {
			return "", valueErr
		}
		return "", ValueInternalSeverError
	}
	return model.UserId(user.Id), nil
}

func onError(w http.ResponseWriter, r *http.Request, err string, status int32) {
	resp := NewErrorResponse(err, status)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(int(status))
	_ = json.NewEncoder(w).Encode(resp)
}
