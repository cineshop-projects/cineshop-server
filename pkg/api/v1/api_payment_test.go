package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	"golang.org/x/crypto/bcrypt"
)

var (
	paymentApiController = &PaymentApiController{service: NewPaymentApiService()}
)

func TestPaymentValidationPost_InternalServerError_Context(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "paymentvalidationpostinternalservererrorcontext@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	cart := addApples(t, user.Id)

	version, _ := cart.LastUpdate.MarshalJSON()
	var jsonStr = []byte(`{"cartVersion":` + string(version) + `,` +
		`"shippingAddress":{},"billingAddress":{},"creditCard":{"cardHolder":"John Doe","cardNumber":"1234567890123456","expiryDate":"12/99","cvv":"987"}}`)

	req, err := http.NewRequest("POST", "/v1/payment/validation", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// The user is not inserted into the req context, thus the StatusInternalServerError

	r := httptest.NewRecorder()

	handler := newTestRouter(paymentApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}

	if r.Body.String() != "" {
		t.Errorf("handler returned unexpected body: got %v want %v",
			r.Body.String(), "")
	}
}

func TestPaymentValidationPost_BadRequest(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "paymentvalidationpostbadrequest@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	// The cart is empty, thus the StatusBadRequest

	version, _ := time.Now().MarshalJSON()
	var jsonStr = []byte(`{"cartVersion":` + string(version) + `,` +
		`"shippingAddress":{},"billingAddress":{},"creditCard":{"cardHolder":"John Doe","cardNumber":"1234567890123456","expiryDate":"12/99","cvv":"987"}}`)

	req, err := http.NewRequest("POST", "/v1/payment/validation", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(paymentApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusBadRequest
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestPaymentValidationPost_Conflict(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "paymentvalidationpostconflict@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	addApples(t, user.Id)

	// The cart version is different from the cart last update, thus the StatusConflict
	version, _ := time.Now().MarshalJSON()
	var jsonStr = []byte(`{"cartVersion":` + string(version) + `,` +
		`"shippingAddress":{},"billingAddress":{},"creditCard":{"cardHolder":"John Doe","cardNumber":"1234567890123456","expiryDate":"12/99","cvv":"987"}}`)

	req, err := http.NewRequest("POST", "/v1/payment/validation", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(paymentApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusConflict {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusConflict)
	}

	var actual Error
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	var expected int32 = http.StatusConflict
	if actual.Code != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual.Code, expected)
	}
}

func TestPaymentValidationPost_OK(t *testing.T) {
	password, err := bcrypt.GenerateFromPassword([]byte("admin1234"), bcrypt.DefaultCost)
	if err != nil {
		t.Fatal(err)
	}

	user, err := dao.GetUserDao().Insert(model.User{
		Id:        "id",
		FirstName: "admin",
		LastName:  "root",
		Email:     "paymentvalidationpostok@test.test",
		Password:  string(password),
	})
	if err != nil {
		t.Fatal(err)
	}

	cart := addApples(t, user.Id)

	version, _ := cart.LastUpdate.MarshalJSON()
	var jsonStr = []byte(`{"cartVersion":` + string(version) + `,` +
		`"shippingAddress":{},"billingAddress":{},"creditCard":{"cardHolder":"John Doe","cardNumber":"1234567890123456","expiryDate":"12/99","cvv":"987"}}`)

	req, err := http.NewRequest("POST", "/v1/payment/validation", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(context.WithValue(req.Context(), UserDetailsProperty, user))

	r := httptest.NewRecorder()

	handler := newTestRouter(paymentApiController)
	handler.ServeHTTP(r, req)

	if status := r.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var actual PaymentStatus
	if err = json.Unmarshal(r.Body.Bytes(), &actual); err != nil {
		t.Fatal(err)
	}

	if actual.TransactionId == "" {
		t.Errorf("handler returned unexpected TransactionId: got %v want %v",
			actual.TransactionId, "non empty string")
	}

	if actual.Status != string(model.StatusApprove) &&
		actual.Status != string(model.StatusHardDecline) &&
		actual.Status != string(model.StatusSoftDecline) {
		t.Errorf("handler returned unexpected Status: got %v want %v",
			actual.Status, string(model.StatusApprove)+"|"+
				string(model.StatusHardDecline)+"|"+
				string(model.StatusSoftDecline))
	}
}
