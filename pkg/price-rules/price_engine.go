package price_rules

import (
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type PriceRulesEngine interface {
	EvaluateAndUpdate(*model.Cart) error
}

var engine PriceRulesEngine

func init() {
	engine = newSimplePriceRulesEngine()
}

func GetPriceRulesEngine() PriceRulesEngine {
	return engine
}
