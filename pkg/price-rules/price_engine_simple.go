package price_rules

import (
	"math"
	"time"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	init_shop "gitlab.com/cineshop-projects/cineshop-server/pkg/init-shop"
)

type simplePriceRulesEngine struct{}

func newSimplePriceRulesEngine() *simplePriceRulesEngine {
	return &simplePriceRulesEngine{}
}

func (*simplePriceRulesEngine) EvaluateAndUpdate(cart *model.Cart) error {
	total, saleTotal := .0, .0
	var appleQuantity, bananaQuantity, pearQuantity, orangeQuantity, bananaPearSetQuantity int32
	var bananaItemId, pearItemId, bananaPearSetItemId int32
	for itemId, item := range cart.Items {
		total += item.Price * float64(item.Quantity)
		switch item.Id {
		case init_shop.Apple.Id:
			appleQuantity = item.Quantity
		case init_shop.Banana.Id:
			bananaQuantity = item.Quantity
			bananaItemId = itemId
		case init_shop.Pear.Id:
			pearQuantity = item.Quantity
			pearItemId = itemId
		case init_shop.Orange.Id:
			orangeQuantity = item.Quantity
		case init_shop.BananaPearSet.Id:
			bananaPearSetQuantity = item.Quantity
			bananaPearSetItemId = itemId
		default:
			saleTotal += item.Price * float64(item.Quantity)
		}
	}

	saleTotal += appleRule(appleQuantity)
	saleTotal += bananaPearSetRule(cart, bananaItemId, pearItemId, bananaPearSetItemId,
		bananaQuantity, pearQuantity, bananaPearSetQuantity)
	saleTotal += applyOrangeRule(cart, orangeQuantity)

	cart.Total = round(total)
	cart.SaleTotal = round(saleTotal)
	cart.LastUpdate = time.Now()

	return nil
}

func appleRule(appleQuantity int32) float64 {
	//  If 7 or more apples are added to the cart, a 10% discount is applied to all apples.
	price := init_shop.Apple.Price * float64(appleQuantity)
	if appleQuantity >= 7 {
		price *= .9
	}
	return price
}

func bananaPearSetRule(cart *model.Cart, bananaItemId, pearItemId, bananaPearSetItemId int32,
	bananaQuantity, pearQuantity, bananaPearSetQuantity int32) float64 {
	// For each set of 4 pears and 2 bananas, a 30% discount is applied, to each set.
	// These sets must be added to their own cart item entry.
	// If pears or bananas already exist in the cart, this discount must be recalculated when new pears or bananas are added.
	var newBananaPearSetsQuantity int32
	if bananaQuantity > 0 && pearQuantity > 0 {
		pearQuartets := pearQuantity / 4
		bananaDuets := bananaQuantity / 2
		newBananaPearSetsQuantity = int32(math.Min(float64(pearQuartets), float64(bananaDuets)))
		bananaQuantity = bananaQuantity - newBananaPearSetsQuantity*2
		pearQuantity = pearQuantity - newBananaPearSetsQuantity*4
	}

	if newBananaPearSetsQuantity > 0 {
		var bananaPearSets model.Item
		if bananaPearSetQuantity > 0 {
			bananaPearSets = cart.Items[bananaPearSetItemId]
			bananaPearSets.Quantity += newBananaPearSetsQuantity
		} else {
			bananaPearSets = model.Item{
				Product:  init_shop.BananaPearSet,
				Quantity: newBananaPearSetsQuantity,
			}
			bananaPearSetItemId = model.GenerateInt32Id()
		}
		cart.Items[bananaPearSetItemId] = bananaPearSets

		updateItemQuantity(cart, bananaItemId, bananaQuantity)
		updateItemQuantity(cart, pearItemId, pearQuantity)
		bananaPearSetQuantity += newBananaPearSetsQuantity
	}

	price := float64(bananaPearSetQuantity) * init_shop.BananaPearSet.Price * .7
	if bananaQuantity > 0 {
		price += init_shop.Banana.Price * float64(bananaQuantity)
	}
	if pearQuantity > 0 {
		price += init_shop.Pear.Price * float64(pearQuantity)
	}

	return price
}

func applyOrangeRule(cart *model.Cart, orangeQuantity int32) float64 {
	// A coupon code can be used to get a 30% discount on oranges, if applied to the cart, otherwise oranges are full price.
	price := init_shop.Orange.Price * float64(orangeQuantity)
	if cart.Coupon != "" {
		price *= .7
	}
	return price
}

func updateItemQuantity(cart *model.Cart, itemId int32, quantity int32) {
	if quantity == 0 {
		delete(cart.Items, itemId)
	} else {
		items := cart.Items[itemId]
		items.Quantity = quantity
		cart.Items[itemId] = items
	}
}

func round(price float64) float64 {
	return math.Floor(price*100) / 100
}
