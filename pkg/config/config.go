package config

import (
	"os"
	"strconv"
	"strings"
)

const (
	portKey = "CINESHOP_SERVER_PORT"

	jwtSecret = "CINESHOP_JWT_SECRET"

	allowedOriginsKey = "CINESHOP_ALLOWED_ORIGINS"
)

type Config struct {
	Port           int
	JWTSecret      string
	AllowedOrigins []string
}

var config Config

func init() {
	portConfig()
	jwtSecretConfig()
	allowedOrigins()
}

func GetConfig() Config {
	return config
}

func portConfig() {
	config.Port = getInt(portKey, 8080)
}

func jwtSecretConfig() {
	config.JWTSecret = getString(jwtSecret, "ThisIsTheSuperSecretKey")
}

func allowedOrigins() {
	tokens := strings.Split(os.Getenv(allowedOriginsKey), ",")

	for _, token := range tokens {
		if len(token) > 0 {
			config.AllowedOrigins = append(config.AllowedOrigins, token)
		}
	}

	// Default value
	if len(config.AllowedOrigins) == 0 {
		config.AllowedOrigins = append(config.AllowedOrigins, "http://localhost:3000")
	}
}

func getString(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}

func getInt(key string, defaultValue int) int {
	if value, ok := strconv.Atoi(os.Getenv(key)); ok == nil {
		return value
	}
	return defaultValue
}
