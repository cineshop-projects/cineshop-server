version ?= 0.9.0

all: publish-image

build-image:
	docker build -t cineshop-server .

publish-image: build-image
	docker tag cineshop-server atefn/cineshop-server:$(version)
	docker push atefn/cineshop-server:$(version)

.PHONY: build-image publish-image all