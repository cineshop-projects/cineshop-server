/*
 * Cineshop
 *
 * Cineshop's fruit shop
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/cineshop-projects/cineshop-server/pkg/config"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"

	_ "github.com/asaskevich/govalidator"
	"github.com/gorilla/handlers"

	api "gitlab.com/cineshop-projects/cineshop-server/pkg/api/v1"
)

func main() {
	port := config.GetConfig().Port

	defer impl.Cleanup()

	AnonymousUsersApiService := api.NewAnonymousUsersApiService()
	AnonymousUsersApiController := api.NewAnonymousUsersApiController(AnonymousUsersApiService)

	AuthenticatedUsersApiService := api.NewAuthenticatedUsersApiService()
	AuthenticatedUsersApiController := api.NewAuthenticatedUsersApiController(AuthenticatedUsersApiService)

	CartApiService := api.NewCartApiService()
	CartApiController := api.NewCartApiController(CartApiService)

	CartItemsApiService := api.NewCartItemsApiService()
	CartItemsApiController := api.NewCartItemsApiController(CartItemsApiService)

	KubernetesProbesApiService := api.NewKubernetesProbesApiService()
	KubernetesProbesApiController := api.NewKubernetesProbesApiController(KubernetesProbesApiService)

	PaymentApiService := api.NewPaymentApiService()
	PaymentApiController := api.NewPaymentApiController(PaymentApiService)

	ProductsApiService := api.NewProductsApiService()
	ProductsApiController := api.NewProductsApiController(ProductsApiService)

	router := api.NewRouter(AnonymousUsersApiController.WithLog(), KubernetesProbesApiController,
		AuthenticatedUsersApiController.WithAuthentication().WithLog(),
		CartApiController.WithAuthentication().WithLog(),
		CartItemsApiController.WithAuthentication().WithLog(),
		PaymentApiController.WithAuthentication().WithLog(),
		ProductsApiController.WithAuthentication().WithLog())

	handler := handlers.RecoveryHandler()(router)

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins(config.GetConfig().AllowedOrigins)
	methodsOk := handlers.AllowedMethods([]string{"GET", "DELETE", "POST", "PUT", "OPTIONS"})

	server := &http.Server{
		Handler:      handlers.CORS(headersOk, originsOk, methodsOk)(handler),
		Addr:         fmt.Sprintf(":%v", port),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		log.Printf("Cineshop server starting on port %v...", port)
		_ = server.ListenAndServe()
	}()

	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-interruptChan

	log.Println("Cineshop server shutting down...")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()
	_ = server.Shutdown(ctx)
	os.Exit(0)
}
