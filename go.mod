module gitlab.com/cineshop-projects/cineshop-server

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535
	github.com/auth0/go-jwt-middleware v0.0.0-20200507191422-d30d7b9ece63
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	gitlab.com/cineshop-projects/cineshop-data-access v0.0.0-20200905220159-8a09a1240bdb
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
)
